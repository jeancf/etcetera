#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# Command line executable

"""
    Copyright (C) 2019  Jean-Christophe Francois

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import argparse
import configparser
import logging

# Verify if optional dependency for logging to journald is present
systemd_present = None
try:
    from cysystemd.journal import JournaldLogHandler
    systemd_present = True
except ModuleNotFoundError:
    systemd_present = False

# Confirm that we have root privileges
if os.geteuid() != 0:
    print('ERROR: You do not have elevated privileges. Try "sudo etcetera"')
    sys.exit(-1)

CONFIG_FILE_LOCATION = '/etc/etcetera.conf'

# Add location of etcetera modules to system path
sys.path.append('/usr/lib/etcetera')

from commands import *

# Check that config file exists
config = None
if os.path.isfile(CONFIG_FILE_LOCATION):
    # Parse config file
    config = configparser.ConfigParser()
    config.read(CONFIG_FILE_LOCATION)
else:
    print('ERROR: config file ' + CONFIG_FILE_LOCATION + ' not found.')
    exit(-1)

# Configure command line arguments parser
cmdline = argparse.ArgumentParser(prog='etcetera', description='Config file management with a touch of wisdom.')

cmdline.add_argument('-v', '--version', action='store_true')
cmdline.add_argument('-i', '--info', action='store_true',
                     help='Display info about Etcetera config and state')
cmdline.add_argument('-l', '--list', action='store_true',
                     help='Display list of files managed by Etcetera')
cmdline.add_argument('-m', '--manage', metavar='FILENAME|DIRNAME', action='store',
                     help='Full path of file to manage.\
                           If path is a directory, manage all files in directory and subdirectories')
cmdline.add_argument('-u', '--unmanage', metavar='FILENAME|DIRNAME', action='store',
                     help='Full path of file to stop managing.\
                          If path is a directory, stop managing all files in directory and sub-directories')
cmdline.add_argument('-p', '--preserve', nargs='+', metavar=('FILENAME', '"NOTE"'), action='store',
                     help='Full path of managed file + note to associate to preserved file '
                          '(surrounded by quote marks)')
cmdline.add_argument('-r', '--restore', metavar='FILENAME', action='store',
                     help='Full path to managed file for which to restore a previous version')
cmdline.add_argument('-s', '--status', metavar='FILENAME', action='store',
                     help='Full path of managed file for which to display status')
cmdline.add_argument('-c', '--changes', metavar='FILENAME', action='store',
                     help='Full path of managed file for which to display diff with a previous version')
cmdline.add_argument('-e', '--edit', metavar='FILENAME', action='store',
                     help='Full path of managed file for which to edit the comment on a preserved version')
cmdline.add_argument('-d', '--delete', metavar='FILENAME', action='store',
                     help='Full path of managed file for which to delete a preserved version')

# Configure logging to system journal
journal = logging.getLogger('etcetera')
if systemd_present:
    journal.addHandler(JournaldLogHandler())
journal.setLevel(logging.INFO)

# convert symlinks to hardlinks if necessary
convert_sym_to_hard_links(config, journal)

# Parse command line arguments
args = vars(cmdline.parse_args())  # convert namespace object to dictionary


# Respond to commands
try:
    if args['list']:
        do_display_list(config)
    elif args['manage'] is not None:
        do_manage(config, args['manage'])
    elif args['unmanage'] is not None:
        do_unmanage(config, args['unmanage'])
    elif args['preserve'] is not None:
            do_preserve_file(config, args['preserve'])
    elif args['restore'] is not None:
        do_restore_file(config, args['restore'])
    elif args['status'] is not None:
        do_display_file_status(config, args['status'])
    elif args['changes'] is not None:
        do_display_changes(config, args['changes'])
    elif args['edit'] is not None:
        do_edit_comment(config, args['edit'])
    elif args['delete'] is not None:
        do_delete_commit(config, args['delete'])
    elif args['info']:
        do_display_info(config)
    elif args['version']:
        do_display_version()
    else:
        print('Check available commands with "etcetera --help"')
except KeyboardInterrupt:
    print("\nInterrupted by user")
    sys.exit(-1)
