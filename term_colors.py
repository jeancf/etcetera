# -*- coding: utf-8 -*-
# Escape codes for coloring output

"""
    Copyright (C) 2019  Jean-Christophe Francois

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

class Colors:
    BOLD      = '\033[1m'
    OKBLUE    = '\033[94m'
    OKGREEN   = '\033[92m'
    WARNING   = '\033[93m'
    FAIL      = '\033[91m'
    UNDERLINE = '\033[4m'
    ENDC      = '\033[0m'

class NoColors:
    BOLD      = ''
    OKBLUE    = ''
    OKGREEN   = ''
    WARNING   = ''
    FAIL      = ''
    UNDERLINE = ''
    ENDC      = ''
