# Roadmap for etcetera #

## Version 0.1 ##

This version is not functional yet. It does not save copies of the config file every time it is saved.

### --manage FILENAME ###

Take over file for tracking.

* Verify that file is valid
* Copy file over to shadow directory
* Replace file in original location by a symlink to the file in shadow directory
* Save copy of file with .etc_orig extension

**DONE**

### --unmanage FILENAME ###

Reinstate file in its original location.

* Replace symlink in original location by file from shadow directory
* Delete all related files in the shadow directory
* Delete empty directories in the path within the shadow directory
* Handle option to restore .orig file along with shadow file

**DONE**

### --list ###

List all files currently managed by etcetera

**DONE**

## Version 0.2 ##

### Split etcetera_mod.py in `commands.py` and `functions.py` ###

**DONE**

### Add functions in toolbox ###

**DONE**

### --manage FILENAME ###

* Add .COMMIT version of the file

**DONE***

### --commit FILENAME ###

* Verify that there are differences between file and its .COMMIT (toolbox function)
* If there aren't, tell the user
* If there are, save copy of file under `.COMMIT` and `.COMMIT[timestamp]`
* Count the number of `.COMMIT[timestamp]` files present. If there are more than allowed delete the oldest.

**DONE**

### --revert FILENAME ###

* Propose list to pick from
* Overwrite managed file with the chosen .COMMIT version

**DONE**

### --status FILENAME ###

* Check that symlink points to file with same name in shadow location
* Mention if there are uncommitted changes to file
* Mention if .ORIG file is available
* List .COMMIT and .ORIG files

**DONE**

### --info ###

Provide useful information about etcetera configuration and state.

* Location of configuration file
* Original locations being monitored
* Location of shadow directory
* Number of managed files
* Max number of backups preserved for each file
* Whether `.ORIG` files are preserved

**DONE**

## Version 0.3 ##

### --commit FILENAME COMMENT ###

Capture and save a comment string to display when showing list of commits

**DONE**

### --revert FILENAME ###

* Display comment alongside list of available commits to revert

**DONE**

### --status FILENAME ###

* Display comment alongside list of available commits to revert

**DONE**

## Version 0.4 ##

### Flag that a file has changed even if only mode/owner/group has changed ###

**DONE**

### Clarify vocabulary ###

* manage/unmanage
* original/shadow
* rev/revert

**DONE**

### Write initial documentation ###

**DONE**

### Write (un)install.sh ###

**DONE**

### Remote on github ###

**DONE**

## Version 0.5 ##

### Reverse order of list of commits ###

So that older commits always keep the same order number (original file = 1)

**DONE**

### gain elevated privileges if not already available

To avoid having to type `sudo` at every invocation

**DONE**

### display user/group/mode in commit list

**DONE**

### When reverting, mention if the original file has uncommited changes

If the file is reverted these changes will be lost

**DONE**

## Version 0.6 ##

Bug fix release

## Version 0.7 ##

### Review command-line parameters syntax ###

Remove --note parameter and integrated into --commit FILENAME \["COMMIT NOTE" ...\]
(The ellipsis cannot be removed because argparse does not allow to specify *one or two* 
arguments. It is either *one* or *at least one*)

**DONE**

### launch external diff application to compare commits ###

Implement

    etcetera --diff FILENAME

presents the list of commits and asks for its number. Shows the diff between current file and selected commit.

**DONE**

## Version 0.8 ##

### Add --version command ###

**DONE**

### List files with uncommitted changes

Add a flag to output of --list command 

**DONE**

### --manage FILENAME|FOLDER ###

Support specifying directories instead of individual files (e.g. `/etc/ssh`)
Use "Are you sure?" confirmation
Documentation

**DONE**

### --unmanage FILENAME|FOLDER ###

Support specifying directories instead of individual files (e.g. `/etc/ssh`)
Use "Are you really sure?" confirmation
Documentation

**DONE**

### Sort output of --list command ###

**DONE**

### Modify access to config keys ###
use SECTION.get(key, default) to prevent exception when key is missing

**DONE**

## Version 0.9 ##

### Basic logging to journald ###

Log to systemd journal using systemd.journal python module
Used only for some critical operations like converting links in upgrade to 0.9

**DONE**

### Make logging to journald optional ###
Skip if systemd module is not installed

**DONE**

### Use hardlink instead of symlink ###

Utilities like etc-update (used by gentoo, archlinux) will overwrite the config
file with a new version. If the config file has been replaced by a symlink (as
etcetera does) the symlink gets overwritten.

By using a hardlink instead of a symlink to the the managed file this problem
disappears. The hardlink gets overwritten but the filename in the managed
location still points to the same inode that got modified by the
overwrite. 

The first time 0.9 is run it will check if symlinks or hardlinks are used and
convert to hardlinks if necessary.

**DONE**

### Update terminology ###

* Symlink disappears
* Managed file = file instance in original location
* Linked file = file instance in managed location

**DONE**

### Change message colors ###

Only color the words ERROR, WARNING, SUCCESS in messages. Display File names
in BOLD.

**DONE**

## Version 1.0 ##

### Support piping for the invocation of the external diff command

Pipe output of diff into `less`

**DONE**

### Fix issue with some package updates

The update to grub-quiet broke etcetera. Apparently it **moved** `/etc/default/grub` to `/etc/default/grub.pacsave`
which caused the managed file to remain a link to that file and no longer to `/etc/default/grub`

> When doing initial verification, if linked_file is found but with different inode than managed_file, delete
linked_file and recreate it as a hardlink pointing to linked_file. Do this for `--list` 

**DONE**

### Verify that path supplied is full path ###

If relative path is supplied issue better error than "file is not in managed location"

**DONE**

### Command name changes

Rename
* `--revert` to `--restore`  **DONE**
* `--diff` to `--changes`  **DONE**
* `--commit` to `--preserve`  **DONE**

### Edit comment for a given commit ###

`-e` for `--edit`

**DONE**

### delete a preserved version ###

`-d` for `--delete`

**DONE**

### Rewrite help screen ###

**DONE**

### do not allow edit/delete of original file ###
If there is an original file, do not let user select it for edit/delete

Manage situation where there is no preserved version (no original file) in `display_file_list()` as well as when calling
`--status`, `--edit` and `--delete`.

> Do **not** give the option to **not** preserve original file (remove config file option).
With this, the list of preserved files will never be empty.
If only (original file) is in the list, do not ask for input in `--edit` and `--delete`.
Do not allow the user to select `1` in the list of commits for `--edit` and `--delete`.

**DONE**

### why does ctrl+c in --edit throw python exception? ###

Fixed by catching KeyboardInterrupt a second time at etcetera level

**DONE**

## Later ##

### implement tests ###

Start with toolbox functions

### Capture login of the user that preserved the changes ###

Store it in .COMMENT file