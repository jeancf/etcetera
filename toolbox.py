# -*- coding: utf-8 -*-
# Library of support functions

"""
    Copyright (C) 2019  Jean-Christophe Francois

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import time
import shutil
import glob
import stat
import pwd
import filecmp
from term_colors import Colors
from term_colors import NoColors


ETCETERA_VERSION = '1.0'
CONST_TIMESTAMP_FORMAT_STRING = '_%Y-%m-%d_%H-%M-%S'


def is_in_original_locations(config, file):
    """
    Verify if file is located in directory listed under ORIGINAL_LOCATIONS in config file
    :param config:  Configuration object
    :param file:    full path + name of file or symlink to check
    :return: True or False
    """
    locations = config['MAIN'].get('ORIGINAL_LOCATIONS', '/etc').split(' ')
    valid = False
    for loc in locations:
        if loc != '' and file.startswith(loc):
            valid = True
    return valid


def is_in_blacklist(config, file):
    """
    Verify if file is listed directly or in a directory under BLACKLIST in config file
    :param config:  Configuration object
    :param file:    full path + name of file or symlink to check
    :return: True or False
    """
    blacklist = config['MAIN'].get('BLACKLIST', '').split(' ')
    for blacklisted in blacklist:
        if blacklisted != '' and file.startswith(blacklisted):
            return True
    return False


def is_managed(config, managed_file, show=True, log=None):
    """
    Verify that the symlink is correctly managed. If it is not, print an error and return False
    :param config:  Configuration object
    :param managed_file: full path + name of hardlink to check
    :param show: whether to display error messages or not
    :param log: logging object to record messages to
    :return: True or False
    """

    # Get color object for terminal output
    if show:
        col = get_colors(config)

    # Check that managed_file is a file or a directory
    if not os.path.isfile(managed_file) and not os.path.isdir(managed_file):
        if show:
            print(col.FAIL + 'ERROR: ' + col.ENDC + col.BOLD + managed_file + col.ENDC + ' does not exist')
        return False

    # Check that managed file is a full path name
    if not managed_file.startswith('/'):
        if show:
            print(
                col.FAIL + 'ERROR: ' + col.ENDC + col.BOLD + managed_file + col.ENDC + ' is not a full path')
        return False

    # Check if managed_file is in allowed original locations
    if not is_in_original_locations(config, managed_file):
        if show:
            print(col.FAIL + 'ERROR: ' + col.ENDC + col.BOLD + managed_file + col.ENDC + ' is not in an allowed original location')
        return False

    # Check that a file with same name as managed file exists in managed location
    linked_file = config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera').rstrip('/') + managed_file
    if not os.path.isfile(linked_file):
        if show:
            print(col.FAIL + 'ERROR:' + col.ENDC + col.BOLD + managed_file + col.ENDC + ' is not managed')
        return False

    # Check that linked file is a hardlink to managed file. If not, make it so.
    # This is to resolve the issue of a managed file being moved and replaced by another one
    if not have_same_inode(managed_file, linked_file):
        os.remove(linked_file)
        os.link(managed_file, linked_file)
        if log is not None:
            log.warning(linked_file + " restored as hardlink to " + managed_file)

    # Check that the corresponding .COMMIT is present
    if not os.path.isfile(linked_file):
        if show:
            print(col.FAIL + 'ERROR:' + col.ENDC + ' shadow file does not exit in managed location')
        return False

    return True


def copy_file_with_stats(source, destination):
    """
    Copy file including user:group and mode
    :param source:      Source path and file name
    :param destination: Destination path
    :return:
    """
    shutil.copy2(source, destination)
    # Make sure file stats are identical as they are used for change detection
    shutil.copystat(source, destination)
    # Apply user:group of original file to copy
    st = os.stat(source)
    os.chown(destination, st[stat.ST_UID], st[stat.ST_GID])


def remove_empty_directories(config, directory):
    """
    Remove empty directories as far up directory as possible
    :param config:    Configuration object
    :param directory: Full path of directory to consider
    :return:
    """
    # Verify that we are within MANAGED_LOCATION
    managed_location = config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera').rstrip('/')
    if directory.startswith(managed_location):
        path = directory.rstrip('/')
        while path != managed_location:
            if len(os.listdir(path)) == 0:  # Directory is empty
                os.rmdir(path)  # No risk of apocalypse as rmdir only works on empty directories
            path = os.path.dirname(path)


def gen_timestamp():
    """
    Get localtime and format timestamp string to append to file e.g. 20180105_105622
    :param:
    :return: string formatted to be used in files extension
    """
    return time.strftime(CONST_TIMESTAMP_FORMAT_STRING, time.localtime())


def get_timestring_from_timestamp(timestamp):
    """
    Returns  string with date and time from timestamp e.g. "Fri Jan  5 10:56:22 2018"
    :param:  timestamp created with get_timestamp()
    :return: string with date and time from timestamp
    """
    return time.asctime(time.strptime(timestamp, CONST_TIMESTAMP_FORMAT_STRING))


def get_file_list(config, managed_file):
    """
    Returns list of dicts with filename, a string with date and time from timestamp, user, group, mode
    :param:  config: configuration object
    :param:  symlink: path of the managed file
    :return: list of .COMMIT files and .ORIG file along with their timestamp formatted for display
    """
    linked_file = os.path.join(config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera') + managed_file)
    commit_file = linked_file + '.COMMIT'
    original_file = linked_file + '.ORIG'

    # Build list of files
    file_list = glob.glob(commit_file + '_*')
    file_list.sort(reverse=True)

    # build list of files and dates of COMMIT files
    full_list = []
    i = 0
    for fn in file_list:
        commit_dict = {}
        i += 1

        commit_dict['name'] = fn

        # Get mode of file
        commit_dict['mode'] = stat.filemode(os.stat(fn).st_mode)

        # Get owner
        commit_dict['user'] = pwd.getpwuid(os.stat(fn).st_uid).pw_name
        commit_dict['group'] = pwd.getpwuid(os.stat(fn).st_gid).pw_name

        # Extract timestamp from file name and transform it into a printable string
        commit_dict['timestring'] = get_timestring_from_timestamp(fn.split('.COMMIT', maxsplit=1)[1])
        full_list.append(commit_dict)

    # Add .ORIG file and date to the list if it exits
    if os.path.isfile(original_file):
        commit_dict = {}
        i += 1

        commit_dict['name'] = original_file

        # Get mode of file
        commit_dict['mode'] = stat.filemode(os.stat(original_file).st_mode)

        # Get owner
        commit_dict['user'] = pwd.getpwuid(os.stat(original_file).st_uid).pw_name
        commit_dict['group'] = pwd.getpwuid(os.stat(original_file).st_gid).pw_name

        # Convert the mtime from file stat into time tuple then into readable string
        commit_dict['timestring'] = time.asctime(time.localtime(os.stat(original_file).st_mtime))
        full_list.append(commit_dict)

    return full_list


def is_different(file1, file2):
    """
    Find out if there are differences between stats of 2 files
    :param file1: first file to compare
    :param file2: second file to compare
    :return: True if any difference is found
    """
    result = True
    file1_stat = os.stat(file1)
    file2_stat = os.stat(file2)

    if filecmp.cmp(file1, file2, shallow=True) and \
            stat.S_IMODE(file1_stat.st_mode) == stat.S_IMODE(file2_stat.st_mode) and \
            file1_stat.st_uid == file2_stat.st_uid and \
            file1_stat.st_gid == file2_stat.st_gid:
        result = False

    return result


def get_colors(config):
    """
    Returns the color object corresponding to the parameter in config file
    :param:   config: configuration object
    :return:  color object
    """
    col = None

    if config['BEHAVIOR'].getboolean('USE_COLORS', False) is True:
        col = Colors()
    else:
        col = NoColors()

    return col


def manage_file(config, managed_file):
    """
    Verify that the file is valid and authorized.
    Create hardlink to managed file under managed location
    Create directories and subdirectories if needed.
    :param config:   configuration object
    :param managed_file: full path of the managed file
    :return: True if successful, False otherwise
    """
    # Get color object for terminal output
    col = get_colors(config)

    # Check that file is not is blacklist
    if is_in_blacklist(config, managed_file):
        print(col.FAIL + 'ERROR:' + col.ENDC + ' File is blacklisted in etcetera.conf')
        return False

    # Check that etc_file is not a symlink
    if os.path.islink(managed_file):
        print(col.WARNING + 'WARNING: ' + col.BOLD + managed_file + col.ENDC + ' is a symlink. It can\'t be managed.')
        return False

    # Check that file does not exist yet in managed location
    linked_file = os.path.join(config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera') + managed_file)
    if os.path.isfile(linked_file):
        print(col.WARNING + 'WARNING: ' + col.ENDC + col.BOLD + managed_file + col.ENDC + ' already managed')
        return False

    # Create file path in managed location if necessary
    managed_path = os.path.dirname(linked_file)
    os.makedirs(managed_path, mode=0o755, exist_ok=True)

    # Create link to managed file in managed location and create copies with .COMMIT and .ORIG (if required) extension
    os.link(managed_file, linked_file)
    copy_file_with_stats(linked_file, linked_file + '.COMMIT')
    copy_file_with_stats(linked_file, linked_file + '.ORIG')

    print(col.OKGREEN + 'SUCCESS:' + col.ENDC + ' Copy of ' + col.BOLD + managed_file + col.ENDC + ' set up in managed location')
    return True


def manage_directory(config, etc_directory):
    """
    List all files in folder and subfolders and call manage_file for each of them
    :param   config:        Configuration object
    :param   etc_directory: Directory of which to manage all files
    :return:
    """
    # iterate files in etc_directory and call manage_file for each
    for directory, subdirectories, files in os.walk(etc_directory, followlinks=False):
        for file in files:
            manage_file(config, os.path.join(directory, file))


def unmanage_file(config, managed_file):
    """
    Restore file or files from managed location to original location.
    If managed_file is a directory, do this for all files in directory and subdirectories
    :param config:        Configuration object
    :param managed_file:  Full path of file or directory containing files
    :return: True if successful, False otherwise
    """
    # Get color object for terminal output
    col = get_colors(config)

    # Check if managed_file is really referring to a managed file
    if not is_managed(config, managed_file, show=False):
        print(col.WARNING + 'WARNING: ' + col.ENDC + col.BOLD + managed_file + col.ENDC + ' is not managed. It can\'t be unmanaged.')
        return False

    linked_file = config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera').rstrip('/') + managed_file

    # Optionally place a copy of .ORIG file in original location
    if os.path.isfile(linked_file + '.ORIG'):
        if config['BEHAVIOR'].getboolean('UNMANAGE_RESTORE_ORIG', False):
            # Put .orig file in managed location
            os.rename(linked_file + '.ORIG', managed_file + '.orig')
            print(
            col.OKGREEN + 'SUCCESS:' + col.ENDC + ' Original version saved under ' + col.BOLD + managed_file + '.orig' + col.ENDC)

        else:
            os.remove(linked_file + '.ORIG')

    # Delete all related files under managed directory
    for f in glob.glob(linked_file + '.COMMIT*'):
        os.remove(f)
    for f in glob.glob(linked_file + '.COMMENT*'):
        os.remove(f)
    os.remove(linked_file)

    # Delete potential empty folders after removal of files
    remove_empty_directories(config, os.path.dirname(linked_file))
    print(col.OKGREEN + 'SUCCESS:' + col.ENDC + ' Managed content related to ' + col.BOLD + managed_file + col.ENDC + ' deleted')
    return True


def unmanage_directory(config, etc_directory):
    """
    List all files in folder and sub-folders and call unmanage_file for each of them
    :param   config:        Configuration object
    :param   etc_directory: Directory of which to unmanage all files
    :return:
    """
    # iterate files in etc_directory and call unmanage_file for each
    for directory, subdirectories, files in os.walk(etc_directory, followlinks=False):
        for file in files:
            unmanage_file(config, os.path.join(directory, file))


def display_managed_file_list(file_list):
    """
    Display the list of preserved files, numbered and ready to collect input
    :param file_list:   List of preserved versions of a managed file
    :return:
    """
    # Define length of user and group strings for alignment
    len_user = 0
    len_group = 0
    for f in file_list:
        if len(f['user']) > len_user:
            len_user = len(f['user'])
        if len(f['group']) > len_group:
            len_group = len(f['group'])
    # Assemble format strings
    ufs = '{:' + str(len_user) + '}'
    gfs = '{:' + str(len_group) + '}'
    i = len(file_list) + 1
    for f in file_list:
        i -= 1
        if '.ORIG' in f['name']:
            # Print original file details
            print(' {:>4}'.format(str(i)) + ' | '
                  + f['timestring'] + ' | '
                  + ufs.format(f['user']) + ' '
                  + gfs.format(f['group']) + ' | '
                  + f['mode'] + ' | '
                  + '(original file)'
                  )

        else:
            # Print details of preserved version
            note = ''
            try:
                nf = open(f['name'].replace('.COMMIT', '.COMMENT'), 'r')
                note = nf.readline()
            except FileNotFoundError:
                pass
            finally:
                nf.close()
            print(' {:>4}'.format(str(i)) + ' | '
                  + f['timestring'] + ' | '
                  + ufs.format(f['user']) + ' '
                  + gfs.format(f['group']) + ' | '
                  + f['mode'] + ' | '
                  + note
                  )


def convert_sym_to_hard_links(config, log):
    """
    Utility function to upgrade. To overcome some limitations, as of V0.9 etcetera works with
    hard links to managed files instead of symbolic links.
    If the managed location does not contain a `.check` file this function will enumerate all
    managed files and overwrite the symlinks to them in the original locations by hard links.
    Finally, it will drop a `.check` file in the managed location.
    :param config:   Configuration file object
    :param log:      Logging object for writing messages to journal
    :return:
    """
    # Verify that managed location exists. If it does not (very first run), create it
    os.makedirs(config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera'), mode=0o755, exist_ok=True)

    # Verify if a file named `.check` is present in MANAGED_LOCATION
    # This file contains the version of etcetera that performed the check
    # and a timestamp. If the file contains version=0.9 or higher stop.
    check_fn = config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera').rstrip('/') + "/.check"
    if os.path.isfile(check_fn):
        check_file = open(check_fn, 'r')
        version_string = check_file.readline()
        if float(version_string.lstrip('version=')) >= 0.9:
            return
        check_file.close()

    # Perform conversion
    log.info('Conversion of symlinks to hardlinks required. Starting...')

    # Enumerate all files in managed location
    for directory, subdirectories, files in os.walk(config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera')):
        for file in files:
            # Consider file only if it does not have the .ORIG, .COMMIT or .COMMENT extension
            if '.ORIG' not in file and '.COMMIT' not in file and '.COMMENT' not in file:
                # Remove managed location from full path to get the original location
                managed_file = os.path.join(directory, file)
                origin = managed_file.replace(config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera'), '')
                # Check that symlink exists in original location
                if os.path.islink(origin):
                    # Check that the symlink points to managed_file
                    if os.readlink(origin) == managed_file:
                        # Check that managed_file and origin are on the same volume
                        if get_partition(origin) == get_partition(managed_file):
                            # Replace symlink with hardlink
                            os.remove(origin)
                            os.link(managed_file, origin)
                            log.info(origin + ' symlink converted to hardlink')
                        else:
                            log.error(origin + ' and ' + managed_file + 'are on different locations')

    # Save `.check` file
    check_file = open(check_fn, 'w')
    check_file.write('version=' + ETCETERA_VERSION + '\n')
    check_file.write('timestamp={}\n'.format(gen_timestamp().lstrip('_')))
    check_file.close()

    log.info('Conversion of symlinks to hardlinks done')


def get_partition(path):
    """
    Return the partition that contains the path
    :param path: Path of which we want to know the partition
    :return:     Partition that contains the path. None if not found
    """
    # read /proc/mounts and build list of mounted partitions
    partitions = []

    with open('/proc/mounts', 'r') as mounts:
        for line in mounts:
            items = line.split()
            if items[0].startswith('/'):  # to match either /dev or // for network mount
                partitions.append((items[0], items[1]))

    # Sort list starting with longest mount point path
    partitions.sort(key=lambda p: len(p[1]), reverse=True)

    partition = None
    for part in partitions:
        # Check if path starts with one of the mount points
        if path.startswith(part[1]):
            partition = part[0]
            break

    return partition


def have_same_inode(path1, path2):
    """
    Return true if both paths are file names pointing to the same inode
    :param path1: Full path to file to compare
    :param path2: Full path to file to compare
    :return: True if both files point to the same inode
    """

    # Check that both paths are files
    if os.path.isfile(path1) and os.path.isfile(path2):
        # Compare
        if os.stat(path1).st_ino == os.stat(path2).st_ino:
            return True

    return False
