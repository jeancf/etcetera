# -*- coding: utf-8 -*-
# Library of commands

"""
    Copyright (C) 2019  Jean-Christophe Francois

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import subprocess

# Add path of Etcetera modules
sys.path.append('/usr/lib/etcetera')

from toolbox import *


def do_display_list(config, show=True):
    """
    Display the list of files currently managed by etcetera
    :param config: configuration object
    :param show: actually print list
    :return: number of files managed, number of changed files
    """

    # Get color object for terminal output
    col = get_colors(config)

    if show:
        print('Files managed by etcetera:')

    n = 0
    num_changed_files = 0
    file_list = []

    # build list of all files in managed location
    for directory, subdirectories, files in os.walk(config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera')):
        for file in files:
            # Consider file only if it does not have the .ORIG, .COMMIT or .COMMENT extension
            if '.ORIG' not in file and '.COMMIT' not in file and '.COMMENT' not in file:
                # Remove managed location from full path to get the original location
                linked_file = os.path.join(directory, file)
                managed_file = linked_file.replace(config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera'), '')
                # Check that the linked file is pointing to same inode as the managed file
                if is_managed(config, managed_file, show=False):
                    n += 1
                    # Check if there are changes to the file that are not preserved
                    marker = ''
                    commit_file = linked_file + '.COMMIT'
                    if is_different(linked_file, commit_file):
                        marker = ' (*)'
                        num_changed_files += 1
                    file_list.append(' ' + col.BOLD + managed_file + marker + col.ENDC)
                    file_list.sort(key=lambda l: (l.lower(), l))

    if show:
        for line in file_list:
            print(line)

        if num_changed_files > 0:
            print('(*) {} file(s) contains changes that are not preserved'.format(num_changed_files))
        print('Number of files managed: ' + col.BOLD + str(n) + col.ENDC)

    return n, num_changed_files


def do_manage(config, etc_object):
    """
    Create hard link to file under managed location.
    Create directories and subdirectories if needed.
    If etc_file is actually a directory, do so for all files in directory.
    :param config:     Configuration object
    :param etc_object: Full path of file or directory containing files
    :return:
    """
    # Get color object for terminal output
    col = get_colors(config)

    # Check if file/directory is in allowed original locations
    if not is_in_original_locations(config, etc_object):
        print(col.FAIL + 'ERROR: ' + col.ENDC + col.BOLD + etc_object + col.ENDC + ' is not in an allowed original location')
        sys.exit()

    # Check if we are dealing with file or directory and manage accordingly
    if os.path.isfile(etc_object):
        success = manage_file(config, etc_object)
        if not success:
            sys.exit()
    elif os.path.isdir(etc_object):
        print(col.WARNING + 'WARNING: ' + col.ENDC + 'This will manage all files within ' + etc_object)
        confirmation = input(col.WARNING + 'WARNING: ' + col.ENDC + 'Are you sure? (y/N): ')
        if confirmation.capitalize() == 'Y':
            manage_directory(config, etc_object)
        else:
            print(col.OKBLUE + 'Operation aborted')
            sys.exit()
    else:
        print(col.FAIL + 'ERROR: ' + col.ENDC + col.BOLD + etc_object + col.ENDC + ' does not exist')
        sys.exit()


def do_unmanage(config, etc_object):
    """
    Restore file or files from managed location to original location.
    If symlink is a directory, do this for all files in directory and subdirectories
    :param config:     Configuration object
    :param etc_object: Full path + name of the symlink to replace with file
    :return:
    """

    # Get color object for terminal output
    col = get_colors(config)

    # Check if file/directory is in allowed original locations
    if not is_in_original_locations(config, etc_object):
        print(col.FAIL + 'ERROR: ' + col.ENDC + col.BOLD + etc_object + col.ENDC + ' is not in an allowed original location')
        sys.exit()

    # Check if we are dealing with file or directory and manage accordingly
    if os.path.isfile(etc_object):
        # Check if managed file has a corresponding hard link under the managed location
        if not is_managed(config, etc_object, show=False):
            print(col.WARNING + 'WARNING: ' + col.ENDC + col.BOLD + etc_object + col.ENDC + ' is not managed. It can\'t be unmanaged.')
            sys.exit()

        print(col.WARNING + 'WARNING:' + col.ENDC + ' This will delete all preserved copies of ' + col.BOLD + etc_object + col.ENDC)
        confirmation = input(col.WARNING + 'WARNING:' + col.ENDC + ' Are you really sure? (y/N): ')
        if confirmation.capitalize() == 'Y':
            success = unmanage_file(config, etc_object)
            if not success:
                sys.exit()

    elif os.path.isdir(etc_object):
        print(col.WARNING + 'WARNING:' + col.ENDC + ' This will unmanage all files within ' + col.BOLD + etc_object + col.ENDC)
        print(col.WARNING + 'WARNING:' + col.ENDC + ' but nothing will be deleted in this directory')
        confirmation = input(col.WARNING + 'WARNING:' + col.ENDC + ' Are you really sure? (y/N): ')
        if confirmation.capitalize() == 'Y':
            unmanage_directory(config, etc_object)
        else:
            print(col.OKBLUE + 'Operation aborted')
            sys.exit()

    else:
        print(col.FAIL + 'ERROR: ' + col.ENDC + col.BOLD + etc_object + col.ENDC + ' does not exist')
        sys.exit()


def do_preserve_file(config, arguments):
    """
    Save a copy of the managed file with .COMMIT extension and a timestamp
    Store note text in file with .COMMENT extension and the same timestamp
    :param config:    Configuration object
    :param arguments: list containing 1 or 2 strings: (1) path to managed file and (2) note text if available
    :return:
    """
    managed_file = arguments[0]
    note = arguments[1] if len(arguments) > 1 else None  # I love python

    # Get color object for terminal output
    col = get_colors(config)

    # Check if file is managed correctly
    if not is_managed(config, managed_file):
        print(col.WARNING + 'Preserve operation aborted' + col.ENDC)
        sys.exit()

    timestamp = gen_timestamp()
    linked_file = config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera').rstrip('/') + managed_file
    commit_file = linked_file + '.COMMIT'
    # Check if there are changes to preserve
    if not is_different(linked_file, commit_file):
        print(col.OKBLUE + "NOTICE:" + col.ENDC + " No unrecorded changes. Nothing to do.")
        sys.exit()

    # Check if a note is required and provided
    if (config['BEHAVIOR'].getboolean('PRESERVE_NOTE_REQUIRED', True) is True) and (note is None):
        print(col.FAIL + 'ERROR:' + col.ENDC + ' Configuration requires a note with every preserved file.')
        sys.exit(0)

    # Create the .COMMIT file
    copy_file_with_stats(linked_file, commit_file + timestamp)
    copy_file_with_stats(linked_file, commit_file)

    # Write message to file
    if note is not None:
        with open(linked_file + '.COMMENT' + timestamp, 'w') as comment_file:
            comment_file.write(note)

    # Delete oldest preserved files in excess of max allowed number
    commit_file_list = glob.glob(commit_file + '_*')
    commit_file_list.sort(reverse=True)
    for i in range(config['BEHAVIOR'].getint('PRESERVE_MAX_SAVES', 5), len(commit_file_list)):
        os.remove(commit_file_list[i])
        # Delete associated .COMMENT file if it exists
        try:
            os.remove(commit_file_list[i].replace('.COMMIT', '.COMMENT'))
        except FileNotFoundError:
            pass

    print(col.OKGREEN + 'SUCCESS:' + col.ENDC + ' version preserved')


def do_restore_file(config, managed_file):
    """
    Display the list of preserved version of the given file for the user to choose.
    Replace the managed file with the version chosen by the user.
    :param config:   Configuration object
    :param managed_file:  Path to the original location of the managed file
    """

    # Get color object for terminal output
    col = get_colors(config)

    # Check if symlink is managed correctly
    if not is_managed(config, managed_file):
        print(col.WARNING + 'Restore operation aborted' + col.ENDC)
        sys.exit(0)

    linked_file = os.path.join(config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera') + managed_file)
    commit_file = linked_file + '.COMMIT'

    # Display list of preserved versions
    print('File was preserved on these dates:')
    file_list = get_file_list(config, managed_file)

    display_managed_file_list(file_list)

    # Check if there are changes that are not preserved
    if is_different(linked_file, commit_file):
        print(col.WARNING + "WARNING: Some changes to the file are not preserved.")
        print("         If you restore now they will be overwritten.\n" + col.ENDC)

    choice = input(col.BOLD + 'Select file version to restore to (1-' + str(len(file_list)) + ', 0 to abort): ' + col.ENDC)

    try:
        num_choice = int(choice)
    except ValueError:
        print(col.FAIL + 'ERROR:' + col.ENDC + ' invalid input')
        sys.exit(0)

    if num_choice == 0:
        print(col.OKBLUE + 'NOTICE: Aborted by user' + col.ENDC)
        sys.exit(0)
    elif 0 < num_choice <= len(file_list):
        # Restore selected file
        file_idx = len(file_list) - num_choice
        copy_file_with_stats(file_list[file_idx]['name'], managed_file)

    else:
        print(col.FAIL + 'ERROR:' + col.ENDC + ' invalid input')
        sys.exit(0)

    print(col.OKGREEN + 'SUCCESS:' + col.ENDC + ' selected version restored')


def do_display_file_status(config, managed_file):
    """
    Display the list of preserved versions for the given file. Mentions if there are some changes to the managed file
    that are not preserved
    :param config:        Configuration object
    :param managed_file:  Full path of the managed file
    """

    # Get color object for terminal output
    col = get_colors(config)

    # Check if hardlink is managed correctly
    if not is_managed(config, managed_file):
        sys.exit()

    linked_file = config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera').rstrip('/') + managed_file
    commit_file = linked_file + '.COMMIT'

    # Display list of preserved versions
    print('File was preserved on these dates:')
    file_list = get_file_list(config, managed_file)

    display_managed_file_list(file_list)

    # Check if there are changes thar are not preserved
    if is_different(linked_file, commit_file):
        print(col.WARNING + "WARNING: " + col.ENDC + "Some changes to the file are not preserved\n")
    else:
        print("File is the same as last preserved version\n")


def do_display_changes(config, managed_file):
    """
    Display changes between managed file and a given preserved version selected by user.
    Use diff command from config file.
    :param config:        Configuration object
    :param managed_file:  Full path of the managed file
    """

    # Get color object for terminal output
    col = get_colors(config)

    # Check if symlink is managed correctly
    if not is_managed(config, managed_file):
        sys.exit(0)

    linked_file = config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera').rstrip('/') + managed_file
    commit_file = linked_file + '.COMMIT'

    # Display list of preserved versions
    print('File was preserved on these dates:')
    file_list = get_file_list(config, managed_file)

    display_managed_file_list(file_list)

    # Check if there are changes that qre not preserved
    if is_different(linked_file, commit_file):
        print(col.WARNING + "WARNING: " + col.ENDC + "Some changes to the file are not preserved\n")
    else:
        print("File is the same as last preserved version\n")

    choice = input(col.BOLD + 'Select file version to display diff from (1-' + str(len(file_list)) + ', 0 to abort): ' + col.ENDC)

    try:
        num_choice = int(choice)
    except ValueError:
        print(col.FAIL + 'ERROR:' + col.ENDC + ' invalid input')
        sys.exit(0)

    if num_choice == 0:
        print(col.OKBLUE + 'NOTICE: Aborted by user' + col.ENDC)
        sys.exit(0)
    elif 0 < num_choice <= len(file_list):
        # Display diff between managed file and selected file
        file_idx = len(file_list) - num_choice
        diff_command = config['BEHAVIOR'].get('DIFF_COMMAND', 'diff -uN {} {}').format(file_list[file_idx]['name'], linked_file)

        # Check if output should be piped into less
        if config['BEHAVIOR'].getboolean('PIPE_DIFF_INTO_LESS', True):
            diff_output = subprocess.run(diff_command.split(), capture_output=True)
            subprocess.run(["less", "-R"], input=diff_output.stdout)
        else:
            subprocess.run(diff_command.split())
    else:
        print(col.FAIL + 'ERROR:' + col.ENDC + ' invalid input')
        sys.exit(0)


def do_edit_comment(config, managed_file):
    """
    Select a given preserved version of the file and provide a new comment string.
    :param config:        Configuration object
    :param managed_file:  Full path of the managed file
    """

    # Get color object for terminal output
    col = get_colors(config)

    # Check if symlink is managed correctly
    if not is_managed(config, managed_file, show=True):
        sys.exit(0)

    linked_file = config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera').rstrip('/') + managed_file
    commit_file = linked_file + '.COMMIT'

    # Display list of preserved versions
    print('File was preserved on these dates:')
    file_list = get_file_list(config, managed_file)

    display_managed_file_list(file_list)

    # Check if there are changes that are not preserved
    if is_different(linked_file, commit_file):
        print(col.WARNING + "WARNING: " + col.ENDC + "Some changes to the file are not preserved\n")
    else:
        print("File is the same as last preserved version\n")

    # Verify that there is at least one preserved version on top of the original file
    if len(file_list) == 1:
        print(col.OKBLUE + "NOTICE:" + col.ENDC + " No preserved version to edit")
        sys.exit(0)

    # Get user selection
    choice = input(col.BOLD + 'Select version to edit comment for (2-' + str(len(file_list)) + ', 0 to abort): ' + col.ENDC)

    try:
        num_choice = int(choice)
    except ValueError:
        print(col.FAIL + 'ERROR:' + col.ENDC + ' invalid input')
        sys.exit(0)

    if num_choice == 0:
        print(col.OKBLUE + "NOTICE:" + col.ENDC + "  Aborted by user")
        sys.exit(0)
    elif 1 < num_choice <= len(file_list):
        # Build name of comment file
        file_idx = len(file_list) - num_choice
        comment_file = file_list[file_idx]['name'].replace('COMMIT', 'COMMENT')
        old_comment = ''
        # Read old comment from file
        try:
            cf = open(comment_file, 'r')
            old_comment = cf.readline()
            cf.close()
        except FileNotFoundError:
            pass

        # Get user input
        print("Press [CTRL]+c to abort")
        print(col.BOLD + "Old comment : " + col.ENDC + old_comment)
        new_comment = input(col.BOLD + "New comment : " + col.ENDC)

        # Open file for writing (truncated) and save
        cf = open(comment_file, "w")
        cf.write(new_comment)
        cf.close()
    else:
        print(col.FAIL + 'ERROR:' + col.ENDC + ' invalid input')
        sys.exit(0)

    print(col.OKGREEN + 'SUCCESS:' + col.ENDC + ' comment modified')


def do_delete_commit(config, managed_file):
    """
    Select a given preserved version of the file and delete it.
    :param config:        Configuration object
    :param managed_file:  Full path of the managed file
    """

    # Get color object for terminal output
    col = get_colors(config)

    # Check if symlink is managed correctly
    if not is_managed(config, managed_file, show=True):
        sys.exit(0)

    linked_file = config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera').rstrip('/') + managed_file
    commit_file = linked_file + '.COMMIT'

    # Display list of preserved versions
    print('File was preserved on these dates:')
    file_list = get_file_list(config, managed_file)

    display_managed_file_list(file_list)

    # Check if there are changes that are not preserved
    if is_different(linked_file, commit_file):
        print(col.WARNING + "WARNING: " + col.ENDC + "Some changes to the file are not preserved\n")
    else:
        print("File is the same as last preserved version\n")

    # Verify that there is at least one preserved version on top of the original file
    if len(file_list) == 1:
        print(col.OKBLUE + "NOTICE:" + col.ENDC + " No preserved version to delete")
        sys.exit(0)

    # Get user selection
    choice = input(col.BOLD + 'Select version to delete (2-' + str(len(file_list)) + ', 0 to abort): ' + col.ENDC)

    try:
        num_choice = int(choice)
    except ValueError:
        print(col.FAIL + 'ERROR:' + col.ENDC + ' invalid input')
        sys.exit(0)

    if num_choice == 0:
        print(col.OKBLUE + "NOTICE:" + col.ENDC + "  Aborted by user")
        sys.exit(0)
    elif 1 < num_choice <= len(file_list):
        print(col.WARNING + 'WARNING:' + col.ENDC + ' This will deleted the selected version')
        confirmation = input(col.WARNING + 'WARNING:' + col.ENDC + ' Are you really sure? (y/N): ')
        if confirmation.capitalize() == 'Y':
            # Build name of comment file
            file_idx = len(file_list) - num_choice
            comment_file = file_list[file_idx]['name'].replace('COMMIT', 'COMMENT')

            # Delete .COMMIT and .COMMENT files
            os.remove(file_list[file_idx]['name'])
            os.remove(comment_file)
            print(col.OKGREEN + 'SUCCESS:' + col.ENDC + ' version deleted')
        else:
            print(col.OKBLUE + 'Operation aborted')
            sys.exit()
    else:
        print(col.FAIL + 'ERROR:' + col.ENDC + ' invalid input')
        sys.exit(0)

def do_display_info(config):
    """
    Display some info related to the configuration and the status of etcetera
    :param config:   Configuration object
    """
    # Get color object for terminal output
    col = get_colors(config)

    # Config file location
    print('Location of config file:')
    print(col.BOLD + ' /etc/etcetera.conf' + col.ENDC)

    # Managed location
    print('Location where files are preserved:')
    print(' ' + col.BOLD + str(config['MAIN'].get('MANAGED_LOCATION', '/var/lib/etcetera')) + col.ENDC)

    # Locations being monitored
    print('Original Location(s) where files can be managed:')
    mon_locs = config['MAIN'].get('ORIGINAL_LOCATIONS', '/etc').split(' ')
    i = 0
    for loc in mon_locs:
        if loc != '':
            print(' ' + col.BOLD + loc + col.ENDC)
    nm, nc = do_display_list(config, show=False)
    # Number of managed files
    print('Number of files managed: ' + col.BOLD + str(nm) + col.ENDC)

    # Number of files with changes
    print('Number of files with changes that are not preserved: ' + col.BOLD + str(nc) + col.ENDC)

    # Max number of backups preserved for each file
    print('Max. number of backups preserved for each file: ' + col.BOLD + str(config['BEHAVIOR'].getint('PRESERVE_MAX_SAVES', 5)) + col.ENDC)


def do_display_version():
    print('etcetera ' + ETCETERA_VERSION)
