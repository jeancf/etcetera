# Test document for etcetera #

## Tests setup ##

```python
>>> import configparser
>>> config = configparser.ConfigParser()
>>> config['MAIN'] = {'SOME_KEY': 'some_value'}
>>> config['BEHAVIOR'] = {'SOME_KEY': 'some_value'}
>>> import toolbox

```

## Tests of functions in toolbox.py ##

### is_in_original_locations() ###

Check if default value (`/etc`) works if no config key is provided

```python
>>> toolbox.is_in_original_locations(config, '/etc/somefile.conf')
True
>>> toolbox.is_in_original_locations(config, '/somefile.conf')
False

```

Now with `ORIGINAL_LOCATION` set correctly in configuration

```python
>>> config['MAIN'] = {'ORIGINAL_LOCATIONS': '/tmp'}
>>> toolbox.is_in_original_locations(config, '/tmp/somefile.conf')
True
>>> toolbox.is_in_original_locations(config, '/somefile.conf')
False

```

### is_in_blacklist ###

```python
>>> config['MAIN'] = {'BLACKLIST': '/tmp/blacklisted'}
>>> toolbox.is_in_blacklist(config, '/tmp/blacklisted/somefile.conf')
True
>>> toolbox.is_in_blacklist(config, '/tmp/somefile.conf')
False

```
